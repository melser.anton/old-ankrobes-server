ankrobes-server
===============
ankrobes-server is a fork of https://github.com/tsudoko/anki-sync-server (which is itself a fork of https://github.com/jdoe0/ankisyncd) which has been adapted for use with the Transcrobes project (https://transcrob.es) in a backwards-incompatible way. The Transcrobes project needed an SRS system but introduces a number of optimisations and features specific to second language learning that are specific to the system.

If you just want a generic Anki-compatible sync server then use https://github.com/tsudoko/anki-sync-server. If you want an all-purpose Anki-compatible sync server with a REST API then use https://github.com/jdoe0/ankisyncd. jdoe0/ankisyncd has not been updated to Anki 2.1 but work has been undertaken in this direction. Both these work well and have reasonable tests.

Documentation
=============
See https://transcrob.es

Status
======
This is PRE-alpha software with KNOWN DATALOSS BUGS. It works, sorta, if you hold it right. There are not yet any tests for the Transcrobes-specific functionality and you should not attempt to use this project for anything like "production" until at least tests have been added.

Development
===========
If you are a (Python) developer learning Chinese, or you really want this to be compatible with learning other languages then your help is needed!

Please look at the [project issues](https://gitlab.com/transcrobes/ankrobes-server/issues) if you need ideas on what to work on - but tests and fixing dataloss bugs are obviously the highest priority. If you don't want to work on tests and you want to implement support for other languages then be warned that merge requests will almost certainly NOT be accepted until proper tests have been written.

## Developer Certificate of Origin
Please sign all your commits by using `git -s`. In the context of this project this means that you are signing the document available at https://developercertificate.org/. This basically certifies that you have the right to make the contributions you are making, given this project's licence. You retain copyright over all your contributions - this sign-off does NOT give others any special rights over your copyrighted material in addition to the project's licence.

## Contributing
See [the website](https://transcrob.es/page/contribute) for more information. Please also take a look at our [code of conduct](https://transcrob.es/page/code_of_conduct) (or CODE\_OF\_CONDUCT.md in this repo).

Installation
============
See ORIG_README.md
