#!/bin/bash

# TODO: FIXME: this should use sqlite3 '.backup mybak.bak' mycoll.anki2
# there is a good risk of backing up corrupt dbs with this!
# see https://stackoverflow.com/questions/25675314/how-to-backup-sqlite-database for example

/bin/tar cjf /var/backups/ankrobes/ankrobes.$(date '+%Y-%m-%d.%H%M%S').tar.bz2 /opt/ankrobes/collections/
